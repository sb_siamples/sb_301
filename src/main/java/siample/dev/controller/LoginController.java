package siample.dev.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import siample.dev.form.LoginForm;

@Controller
public class LoginController {

	@RequestMapping(value="/login", method=RequestMethod.GET) // localhost:8080/login -ra GET -et küldve jelenítsd meg a login.html lapot. 
	public String getLoginForm() {
		return "login";
	}
	
	@RequestMapping(value="/login", method=RequestMethod.POST) // amikor localhost:8080/login -ra POST -ot küldenek, akkor a html formba vitt, gépelt dolgokkal 
	public String login(@ModelAttribute(name="kiki") LoginForm loginFormlet, Model model) { // csinálj egy loginForm osztályú objektumot, és azt tedd be a Model model-be "kiki" attribútumként.
		
	
		System.out.println("POST Http request received: " + model.getAttribute("kiki").getClass());

		
		
		String username = loginFormlet.getUsern();
		String password = loginFormlet.getPassw();
		
		if ("a".equals(username) && "a".equals(password) ) {
			return "home";
		}
		
		model.addAttribute("invalidCredentials", true);
		return "login";
		
	}
}
