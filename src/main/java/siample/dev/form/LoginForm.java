package siample.dev.form;

public class LoginForm {
	
	private String usern;
	private String passw;
	
	public LoginForm() {
		
	}

	public LoginForm(String username, String password) {
		this.usern = username;
		this.passw = password;
	}

	public String getUsern() {
		return usern;
	}

	public void setUsern(String usern) {
		this.usern = usern;
	}

	public String getPassw() {
		return passw;
	}

	public void setPassw(String passw) {
		this.passw = passw;
	}
	
	

}
